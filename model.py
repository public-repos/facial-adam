import torch
import torch.nn as nn
import torch.nn.functional as F

import numpy as np
import pickle as pkl


class MLP(nn.Module):
    """The model referenced in the paper: https://arxiv.org/abs/1504.07550
    It contains 4 layers:
    [{'n_in': 2500, 'activation': 'sigmoid', 'n_out': 1025},
    {'n_in': 1025, 'activation': 'sigmoid', 'n_out': 512},
    {'n_in': 512, 'activation': 'tanh', 'n_out': 64},
    {'n_in': 64, 'activation': 'tanh', 'n_out': 136}]"""

    def __init__(self):
        super(MLP, self).__init__()
        self.layer1 = nn.Linear(2500, 1025, bias=True)
        self.layer2 = nn.Linear(1025, 512, bias=True)
        self.layer3 = nn.Linear(512, 64, bias=True)
        self.layer4 = nn.Linear(64, 136, bias=True)

    def forward(self, x):
        x = self.layer1(x)
        x = torch.sigmoid(x)
        x = self.layer2(x)
        x = torch.sigmoid(x)
        x = self.layer3(x)
        x = torch.sigmoid(x)
        x = self.layer4(x)
        x = torch.tanh(x)

        return x

    def init_params(self, path_model_theano):
        """Initialize the model's parameters using pre-trained model."""
        with open(path_model_theano, 'rb') as f:
            stuff = pkl.load(f)
        params = stuff["params_vl"]

        self.layer1.weight.data.copy_(torch.tensor(params[0]).t())
        self.layer1.bias.data.copy_(torch.tensor(params[1]))

        self.layer2.weight.data.copy_(torch.tensor(params[2]).t())
        self.layer2.bias.data.copy_(torch.tensor(params[3]))

        self.layer3.weight.data.copy_(torch.tensor(params[4]).t())
        self.layer3.bias.data.copy_(torch.tensor(params[5]))

        self.layer4.weight.data.copy_(torch.tensor(params[6]).t())
        self.layer4.bias.data.copy_(torch.tensor(params[7]))


if __name__ == "__main__":
    model = MLP()
    for name, p in model.named_parameters():
        print(name, p.size())

    for idx, m in enumerate(model.modules()):
        print(idx, m)

    print(model.layer1.weight.data.size())
    model.init_params(path_model_theano="model-theano-python3-6/model.pkl")
    torch.save(model.state_dict(), "init-params-pytorch/model.pt")


