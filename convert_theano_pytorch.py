# Convert a Theano network into Pytorch (1.0.0)

import pickle as pkl


# Model from Theano
model_path = "model-theano-python2-7/model.pkl"
with open(model_path, 'rb') as f:
    stuff = pkl.load(f, encoding='latin1')

# the used activations.
activations = ["sigmoid", "sigmoid", "sigmoid", "tanh"]
params = stuff["params_vl"]
for p in params:
    print(p.shape)

print(type(params))
print(len(params))
for e in params:
    print(e.shape)

model_output = "model-theano-python3-6/model.pkl"
with open(model_output, 'wb') as f:
    pkl.dump({"params_vl": params, "activations": activations}, f)


