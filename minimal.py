import torch

from model import MLP

# Instantiate the model.
model = MLP()
# check if there is a gpu. If there are many, you can specify which one: cuda:id.
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

# Initialize the model with pre-trained parameters.
model.load_state_dict(torch.load("init-params-pytorch/model.pt"))
# Move the model to device on which to perform the prediction. Default: 'cpu'.
model.to(device)

# Prediction
nbr_faces = 10
dim = 50*50
input = torch.randn(nbr_faces, dim)  # Input data: a torch tensor.
input = input.to(device)  # move the input data into the device.
output = model(input)  # predict the landmarks.
output = output.cpu().detach().numpy()  # get back to the CPU.
print(output.shape)