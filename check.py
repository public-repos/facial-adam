import pickle as pkl
import numpy as np
import matplotlib.pyplot as plt


path_data = "data/lfpw_tr.pkl"
with open(path_data, 'rb') as f:
    stuff = pkl.load(f, encoding='latin1')

print(type(stuff))
print(stuff[0].keys())
print(stuff[0]["img_gray"].shape)
plt.imshow(stuff[0]["img_gray"])
plt.show()