Project for facial landmark detection for students at L'Université de Rouen Normandie.

### Task:
Detect a set of key points on image faces. [68 landmarks, each one has (x, y) coordinates on the image plan.]

### Model:
- Work based on the publication:  

>@article{sbelharbiarneurocomputing2017,  
  author = {Belharbi, Soufiane and Hérault, Romain and Chatelain, Clément and Adam, Sébastien},  
  title = {Deep Neural Networks Regularization for Structured Output Prediction},  
  journal = {Neurocomputing},  
  year = {2018},  
  volume = {281C},  
  pages = {169-177}}  
- Arxiv: https://arxiv.org/abs/1504.07550
- Use pre-trained model over HELEN dataset using labeled and unlabeled data ([Table 4](https://arxiv.org/abs/1504.07550 )).
- Architecture:
    * Input size: 2500 (50x50)
    * First hidden layer: 1025 (use bias=True, activation: Sigmoid).
    * Second hidden layer: 512 (use bias=True, activation: Sigmoid).
    * Third hidden layer: 64 (use bias=True, activation: Sigmoid).
    * Output layer: 136 (68x2) (use bias=True, activation: Tanh).

### Steps:
1. Load image.
2. Convert image to gray level.
3. Localize face(s).
4. Crop face.
5. Rescale face into (50, 50).
6. Flatten face into an array (2500).
7. Normalize the value of array into [0, 1]. ([min-max normalization](https://en.wikipedia.org/wiki/Feature_scaling))
8. Predict the landmarks using the model. (the shape is represented in a squared plan centered at zero, with the upper 
left coordinate of the plan is (-1, 1)). The first 68 values in the predicted vector are the x's of the landmarks, 
while the last 68 are the y's.
9. Project the output shape into the rescaled face [plan of 50x50].
10. Project the resulted shape into the original image.

### Folders:
* `haarcascades`: contains the `*.xml` Haar face detector. It can be used with OpenCV.
* `init-params-pytorch`: contains the initial parameters of the Pytorch model.
* `model-theano-python2-7`: contains the pre-trained model's parameters issued from Theano in Python 2.7.
* `model-theano-python3-6`: contains the converted parameters in the folder `model-theano-python2-7` into Python 3.6.

### Minimal example for prediction:
See [`minimal.py`](minimal.py).
```python
import torch

from model import MLP

# Instantiate the model.
model = MLP()
# Check if there is a gpu. If there are many, you can specify which one: cuda:id.
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

# Initialize the model with pre-trained parameters.
model.load_state_dict(torch.load("init-params-pytorch/model.pt"))
# Move the model to device on which to perform the prediction. Default: 'cpu'.
model.to(device)

# Prediction
nbr_faces = 10
dim = 50*50
input = torch.randn(nbr_faces, dim)  # Input data of size (10, 2500): a torch tensor.
input = input.to(device)  # move the input data into the device.
output = model(input)  # predict the landmarks.
output = output.cpu().detach().numpy()  # get back to the CPU.
print(output.shape)  # -> (10, 136)

```
### Expected output:
When applying the previously mentioned steps over [Input.jpg](Input.jpg), we obtain something like this:  
![Output](Output.png "Output")

### Requirements:
- Python 3.6.6
- Pytorch 1.0.0 (https://pytorch.org/), for prediction.
- OpenCV 3.1.0, for image processing (install using Anaconda to avoid many issues in Python 3 including cv2.imshow: conda install -c menpo opencv3)

### Questions:
Contact: [sbelharbi.github.io/contact](https://sbelharbi.github.io/contact/)
